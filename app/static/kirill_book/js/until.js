async function hash(string) {
  // hash('foo').then((hex) => console.log(hex));
  const utf8 = new TextEncoder().encode(string);
  const hashBuffer = await crypto.subtle.digest('SHA-256', utf8);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray
    .map((bytes) => bytes.toString(16).padStart(2, '0'))
    .join('');
  return hashHex;
}

function setAuthCookie(token) {
    document.cookie = `auth_token=${token}`;
}

function getAuthCookie() {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; auth_token=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
  return ''
}

