function registerUser() {
    const login = document.getElementById('register_login').value;
    const first_name = document.getElementById('register_first_name').value;
    const second_name = document.getElementById('register_second_name').value;
    const gender = document.getElementById('register_gender').value;
    const city = document.getElementById('register_city').value;
    const football = document.getElementById('register_football').checked;
    const tv = document.getElementById('register_tv').checked;
    const game = document.getElementById('register_game').checked;
    const age = document.getElementById('register_age').value;
    // Пока нет ssl сертификата
    axios.post(`/register`, {
                    login: login,
                    password_hash: document.getElementById('register_password').value,
                    first_name: first_name,
                    second_name: second_name,
                    gender: gender,
                    city: city,
                    age: age,
                    user_hobbies: {
                        football: football,
                        tv: tv,
                        game: game,
                    }
            }).then(
                (response) => {
                    window.location.replace(`${URL_BASE}/?`);
                }
            ).catch(function (error) {
                alert('Такой адресс уже зарегестрирован');
            })
}