import random
import uuid

from django.db import connection

from db_until.random_params import FIRST_NAMES, SECOND_NAME, CITY, GENDER



class StartSetup:
    @classmethod
    def start_installation(cls):
        with connection.cursor() as cursor:
            cursor.execute(
                """
            create table users
            (
                uid           int auto_increment,
                login         varchar(50)                                                   not null,
                password_hash varchar(64)                                                   not null,
                first_name    varchar(64)                                                   not null,
                second_name   varchar(64)                                                   not null,
                age           int                                                           not null,
                gender        ENUM ('Male', 'Female', 'Helicopter', 'Offer') default 'Male' not null,
                city          varchar(64)                                                   not null,
                constraint users_pk
                    primary key (uid)
            );
            
            create unique index users_login_uindex
                on users (login);
            
            create unique index users_uid_uindex
                on users (uid);
            
            create table user_hobbies
            (
                user_uid int                             not null,
                hobby  ENUM ('Football', 'Game', 'TV') not null,
                constraint user_hobbies_users_uid_fk
                    foreign key (user_uid) references users (uid)
                        on update cascade
            );
            
            create table user_auth_token
             (
                 uid   int         null,
                 token varchar(36) not null
             );
            create unique index user_auth_token_uid_uindex
                             on user_auth_token (token);
            
            create unique index user_auth_token_uid_uindex
                             on user_auth_token (uid);            
            """
            )

    @classmethod
    def add_random_users(cls, user_count=1000000):
        with connection.cursor() as cursor:
            for i in range(user_count):
                query = """
                           INSERT INTO users (login, password_hash, first_name, second_name, age, gender, city)
                           VALUES (%s, %s, %s, %s, %s, %s, %s);
                           """
                values = (
                    'random' + str(i)+str(uuid.uuid4()),
                    'password',
                    random.choice(FIRST_NAMES),
                    random.choice(SECOND_NAME),
                    random.randrange(100),
                    random.choice(GENDER),
                    random.choice(CITY),
                )
                cursor.execute(query, values)
                if i % 1000 == 0:
                    print(i)

    @classmethod
    def add_index(cls):
        with connection.cursor() as cursor:
            cursor.execute("""
            create index find__index
                on users (first_name, second_name);
            """)