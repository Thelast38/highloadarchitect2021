from django.db import connection


class UserProfileDb:
    @classmethod
    def profile_data(cls, token):
        with connection.cursor() as cursor:
            query = "SELECT uid FROM user_auth_token WHERE token = %s"
            params = [token]
            cursor.execute(query, params)
            uid = cursor.fetchone()

            query = "SELECT login, first_name, second_name, age, gender, city FROM users WHERE uid = %s"
            params = [uid]
            cursor.execute(query, params)
            result = cursor.fetchone()

            query = "SELECT hobby FROM user_hobbies WHERE user_uid = %s"
            params = [uid]
            cursor.execute(query, params)
            hobbies = cursor.fetchall()
            hobbies_arr = []
            for hobby in hobbies:
                hobbies_arr.append(hobby[0])
            hobbies_str = ";".join(hobbies_arr)
            return {
                "login": result[0],
                "first_name": result[1],
                "second_name": result[2],
                "age": result[3],
                "gender": result[4],
                "city": result[5],
                "hobbies": hobbies_str,
            }
