from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response

from user_profile.db import UserProfileDb


class UserProfile(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "profile.html"

    def get(self, request):
        auth_token = request.COOKIES["auth_token"]
        data = UserProfileDb.profile_data(auth_token)
        return Response(data)
