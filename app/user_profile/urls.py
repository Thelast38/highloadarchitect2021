from django.urls import path

from user_profile.views import user_profile

app_name = "user_profile"

urlpatterns = [
    path("", user_profile.UserProfile.as_view()),
]
