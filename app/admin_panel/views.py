# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response

from db_until.start_setup import StartSetup


class AdminPanel(APIView):
    def post(self, request):
        action = request.data["action"]
        if action == "init_db":
            StartSetup.start_installation()
        elif action == "add_random_users":
            StartSetup.add_random_users()
        elif action == "add_index":
            StartSetup.add_index()
        return Response(status=200, content_type="application/json")
