from django.urls import path

from admin_panel.views import AdminPanel

app_name = "admin_panel"

urlpatterns = [
    path("", AdminPanel.as_view()),
]
