import uuid

from django.db import connection


def uuid_generator():
    return str(uuid.uuid4())


class StartMenuDb:
    @classmethod
    def sing_up(cls, login, password_hash):
        with connection.cursor() as cursor:
            query = "SELECT uid FROM users WHERE login = %s and password_hash = %s"
            params = (login, password_hash)
            cursor.execute(query, params)
            uid = cursor.fetchone()
            if uid:
                token = uuid_generator()
                query = """
                REPLACE  INTO kirill_book.user_auth_token (uid, token)
                VALUES (%s, %s);
                """
                params = (uid, token)
                cursor.execute(query, params)
                return token
            raise ValueError("Неверный логин или пароль")
