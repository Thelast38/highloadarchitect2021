from django.urls import path

from start_menu.views import start_page

app_name = "start_menu"

urlpatterns = [
    path("", start_page.StartPage.as_view()),
]
