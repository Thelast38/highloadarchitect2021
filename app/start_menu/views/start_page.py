from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.views import APIView
from rest_framework.response import Response

from start_menu.db import StartMenuDb


class StartPage(APIView):
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    template_name = "sing_in.html"

    def get(self, request):
        return Response()

    def post(self, request):
        login = request.data["login"]
        password_hash = request.data["password"]

        token = StartMenuDb.sing_up(login, password_hash)
        body = {"token": token}
        return Response(body, status=200, content_type="application/json")
