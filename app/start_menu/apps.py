from django.apps import AppConfig


class StartMenuConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "start_menu"
