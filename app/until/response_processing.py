from rest_framework.response import Response


def setup_cors_response_headers(res):
    # TODO: put origin inside the config.
    res["Access-Control-Allow-Origin"] = "127.0.0.1"
    # TODO: create allow_method decorator.
    res["Access-Control-Allow-Methods"] = "GET,POST,PUT,DELETE,OPTIONS"
    res[
        "Access-Control-Allow-Headers"
    ] = "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
    res["Access-Control-Allow-Credentials"] = "true"
    return res


def cors_response():
    return setup_cors_response_headers(Response(status=204))


def get_success_response(body={}):
    return setup_cors_response_headers(
        Response(body, status=200, content_type="application/json")
    )


def get_error_response(status_code):
    return setup_cors_response_headers(
        Response(status=status_code, content_type="application/json")
    )
