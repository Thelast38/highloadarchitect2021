from django.urls import path

from find_friends.views import find_friends

app_name = "find_friends"

urlpatterns = [
    path("", find_friends.FindFriends.as_view()),
]
