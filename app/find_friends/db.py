from django.db import connection


class UserData:
    def __init__(self):
        self.name = '',
        self.age = 0,
        self.gender = 'Male',
        self.city = 'Gotham'

    @classmethod
    def from_sql_data(cls, sql_data) -> "UserData":
        data_instance = UserData()
        data_instance.name = f"{sql_data[0]} {sql_data[1]}"
        data_instance.age = sql_data[2]
        data_instance.gender = sql_data[3]
        data_instance.city = sql_data[4]
        return data_instance


class FindUsersDb:
    @classmethod
    def find_users(cls, find_str: str):
        with connection.cursor() as cursor:
            first_name, second_name = find_str.split()[:2]
            query = "SELECT first_name, second_name, age, gender, city FROM users WHERE first_name LIKE %s and second_name LIKE %s ORDER BY uid"
            params = list(map(lambda name: name+'%', [first_name, second_name]))
            cursor.execute(query, params)
            results = cursor.fetchall()
            # Ограничил показ пользователей 10
            return [UserData.from_sql_data(sql_data) for sql_data in results][:10]
