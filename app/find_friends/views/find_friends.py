from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response

from find_friends.db import FindUsersDb


class FindFriends(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "find_friends.html"

    def get(self, request):
        find_str = self.request.query_params.get('find')
        data = {
            "find_str": find_str if find_str else "",
            "users": FindUsersDb.find_users(find_str) if find_str else [],
        }
        return Response(data)
