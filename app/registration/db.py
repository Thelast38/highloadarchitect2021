from django.db import connection


class RegistrationDb:
    @classmethod
    def register(cls, data):
        login = data["login"]
        with connection.cursor() as cursor:
            query = "SELECT uid FROM users WHERE login = %s"
            cursor.execute(query, [login])
            result = cursor.fetchall()
            if result:
                raise ValueError("Login is all ready result")

            query = """
            INSERT INTO users (login, password_hash, first_name, second_name, age, gender, city)
            VALUES (%s, %s, %s, %s, %s, %s, %s);
            """
            values = (
                login,
                data["password_hash"],
                data["first_name"],
                data["second_name"],
                int(data["age"]),
                data["gender"],
                data["city"],
            )
            cursor.execute(query, values)

            query = "SELECT uid FROM users WHERE login = %s"
            cursor.execute(query, [login])
            uid = cursor.fetchone()
            for key, value in data["user_hobbies"].items():
                if value:
                    query = """
                    INSERT INTO user_hobbies (user_uid, hobby)
                    VALUES (%s, %s);
                    """
                    values = (uid, key)
                    cursor.execute(query, values)
