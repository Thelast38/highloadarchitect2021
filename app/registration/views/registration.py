from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response

from registration.db import RegistrationDb
from until.response_processing import cors_response


class RegisterPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "register.html"

    def get(self, request):
        return Response()

    def post(self, request):
        data = request.data
        RegistrationDb.register(data)
        return Response()

    def options(self, request, *args, **kwargs):
        return cors_response()
