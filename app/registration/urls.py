from django.urls import path

from registration.views import registration

app_name = "registration"

urlpatterns = [
    path("", registration.RegisterPage.as_view()),
]
